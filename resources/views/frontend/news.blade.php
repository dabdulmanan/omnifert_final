<!doctype html>
<html lang="en">
<head>
  <title>OmniFert | News</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  @php $page = "news" @endphp


  <!-- include navigation -->
  @include("frontend.includes.navigation")
  <!-- include navigation -->



  <!-- top slider section -->
  <section class="news centeritem">
    <div class="container ">
      <div class="row ">
        <div class="col-12 col-md-8 ">
            <img src="img/facedown.svg" alt="" class="facedownlefttt d-none d-sm-block">
          <h1 class="display-4 wc">
            News & <br>   <strong>Media</strong> </h1>
          </div>
        </div>

      </div>
    </section>

    <!-- news and media -->
    <section class="globalspace ">
      <div class="container">


        <div  class="row">


       @foreach ($news as $row)
          
            <div class="col-12 col-md-4  mb-5">

              <div class="entervas-konko mb-3">
                <a href="/news/{{ $row->slug }}">
                  <img src="{{ asset('blog_images/'.$row->pic) }}" alt="{{ $row->title}}" class="img-fluid vasimg">
                  <div class="overlay-light-black"></div>
                  <div class="text-contain px-4">
                  <p class="flagt3 mb-0">{{ $row->title}}</p>
                </div>
              </a>
            </div>
            <p class="product-mini">{!! limit_text($row->content, 250) !!}
            </p>
            <a href="/news/{{ $row->slug }}" class="btn btn-success btn-success2 pl-3 ">View All <i class='ml-5 rrr fa fa-angle-right '></i></a>
          </div>

        @endforeach

     <div class="col-12 text-center">
        {{ $news->links()}}
     </div>




   </div>
 </div>
</section>
<!-- news and media -->



<!-- footer includes -->
@include("frontend.includes.footer")
<!-- footer includes -->

</body>
</html>